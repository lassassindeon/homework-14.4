﻿#include <iostream>
#include <string>

int main()
{
    std::string textForHomework;
    textForHomework = "This is a text for SkillBox homework 14.4";

    std::cout << textForHomework << "\n";
    std::cout << textForHomework.length() << "\n";
    std::cout << textForHomework.front() << "\n";
    std::cout << textForHomework.back() << "\n";
}
